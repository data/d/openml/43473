# OpenML dataset: Hottest-Kaggle-Datasets

https://www.openml.org/d/43473

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data was collected as a course project for the immersive data science course (by General Assembly and Misk Academy).
Content
This dataset is in a CSV format, it consists of 5717 rows and 15 columns, where each row is a dataset on Kaggle and each column represents a feature of that dataset.

title        dataset name
usability        dataset usability rating by Kaggle
numoffiles        number of files associated with the dataset
typesoffiles        types of files associated with the dataset
files_size        size of the dataset files
vote_counts        total votes count by the dataset viewer
medal        reward to popular datasets measured by the number of upvotes (votes by novices are excluded from medal calculation), [Bronze = 5 Votes, Silver = 20 Votes, Gold = 50 Votes]
url_reference        reference to the dataset page on Kaggle in the format: www.kaggle.com/url_reference
keywords        Topics tagged with the dataset
numofcolumns        number of features in the dataset
views        number of views
downloads        number of downloads
downloadperview        download per view ratio
date_created        dataset creation date
last_updated        date of the last update

Acknowledgements
I would like to thank all my GA instructors for their continuous help and support
All data were taken from https://www.kaggle.com , collected on 30 Jan 2021 
Inspiration
Using this dataset, we could try to predict the upcoming datasets uploaded, number of votes, number of downloads, medal type, etc.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43473) of an [OpenML dataset](https://www.openml.org/d/43473). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43473/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43473/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43473/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

